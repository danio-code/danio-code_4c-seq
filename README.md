# 4C-seq analysis from fastqs to visualization

## Demultiplexing (scripts/demultiplex.py)

The first step is to demultiplex the fastq files. They contain 50bp single cell reads from several 4C-seq experiments with different baits. The beginning of the read correspond to the sequence of the primer in the bait followed by the sequence of the interacting fragment. Using the sequence of the primer it is possible to identify the reads coming from the different experiments and split the reads in different fastq files. The sequence of the primer is also removed so that only the parts of the reads coming from the different interacting loci are kept and are then alignable to the reference genome. This is done by a Python2 script that uses the editdist package (https://pypi.org/project/editdist/).  

```
usage: demultiplex.py [-h] -f FASTQ_FILE -c PRIMERS_FILE [-fuzzy MISMATCHES]
                      [-o OUTDIR]

Demultiplex a whole lane fastq file in individual files, removing the primers
sequences.

optional arguments:
  -h, --help         show this help message and exit
  -f FASTQ_FILE      Input fastq file (required)
  -c PRIMERS_FILE    File with primers name and sequence (required)
  -fuzzy MISMATCHES  Allow a number of mismatches when demultiplexing
  -o OUTDIR          Output directory
```

It takes two input files: the fastqfile with the lane to demultiplex and a second file with the information of the primers. It has the same format than the file [primer_list.tsv](./primer_list.tsv), a tsv with three columns for name, primer sequence and genomic coordinate. The genomic coordinate is not used for the demultiplexing but it is used in later stages of the analysis where the same file is required. The script also takes two extra options: one that set the number of mismatches allowed (-fuzzy, we used three for our 4C-seq analysis) and other that set the output directory. We have included an example of a [fastq file](sample_data/fqfiles/raw4C.fq) that contain the 4C-seq experiments of the promoters of the zebrafish genes *eng2a* and *shha*. The primer file that should be used for this example is the [following](sample_data/aux/prim.txt). It produces two fastq files, one for each of the two experiments, with the names that are specified in the primers file. This is an example of how it could be invoked. 

```sh
>> python scripts/demultiplex.py -f sample_data/fqfiles/raw4C.fq -c sample_data/aux/prim.txt -fuzzy 3 -o sample_data/fqfiles/

```

## From demultiplexed files to visualization (scripts/4Cseq_pipe.pl)

The rest of the analysis process is automatized by a perl script. It performs several tasks: (i) creates the restriction map of the reference genome, (ii) maps the reads to the reference genome using Bowtie1 with default parameters except from the -m 1 option that supress reads mapping to more than one location (see [bowtie1 manual](http://bowtie-bio.sourceforge.net/index.shtml)), (iii) assign each of the mapped reads to one of the restriction fragments using [bedtools](https://bedtools.readthedocs.io/en/latest/), (iv) it discards the reads in fragments located +-10kb from the viewpoint (here the genomic coordinate of the primer file is used), (v) it smoothens the signal of each restriction fragment by using a running window of 31 fragments, (vi) it predicts signficant interactions using a poisson model. It requires the [ForkManager](https://metacpan.org/pod/Parallel::ForkManager) module for parallel processing. This is the help message: 

```
Usage: 4Cseq_pipe.pl [options]
NOTE: this script uses Bowtie to map reads to the target genome and Bedtools to assign reads to restriction fragments.
      Bedtools, Bowtie and its index must be installed and available from $PATH environment variable.
Options:
-f         path to folder with already demultiplexed input FASTQ files for each primer.
-c         primers file (must be tabular, three columns with primer name, seq and position chr0:000000)
-i         bowtie index of the target genome
-n         name of the experiment (genome and enzymes used). Next time you can reuse it and -e and -g options are not required
-e         enzymes file (must be tabular, two columns with name and restriction site). Enzymes should be in experimental order.
           Only required the first time that a combination genome/enzymes is used 
-g         target genome in fasta format. Only required the first time that a combination genome/enzymes is used
-p         number of threads to use
-d         only demultiplex the fastq file. This option only requires -f and -c options
-mappath   path to folder with already mapped files for each primer (with .map extension). -i option is not required
```

It takes several inputs: (i) the folder with the demultiplexed fastq files, (ii) the same primer file used for the demultiplexing, (iii) the bowtie1 index of the reference genome to use, (iv) the fasta file with the reference genome, (v) a file with the restriction enzymes used, in the following [format](sample_data/aux/enz.txt) and (vi) a name to save the restriction map of the reference genome, that can be reused when calling the program again. Below is an example of how the program can be invoked: 

```sh
>> cd results
>> perl 4Cseq_pipe.pl \\
-f ../sample_data/fqfiles/Demultiplexed/ \\
-c ../sample_data/aux/prim.txt \\
-i ../sample_data/genome/danRer10 \\
-n ../sample_data/aux/danRer10_DpnII_Csp6I \\ 
-e ../sample_data/aux/enz.txt \\
-g ../sample_data/genome/danRer10.fa \\
-p 20 1>4Clog.out 2>4Clog.err &
```
The program outputs several folders with several intermediate and final files for each of the experiments. First it outputs mapfiles in the default [bowtie1 format](http://bowtie-bio.sourceforge.net/manual.shtml#default-bowtie-output) like the following for [shha](results/mapfiles/shha.map). Then the viewpoint is filtered and the remaining reads are stored in [bed format](results/bedfiles/shha.bed). After this the reads are intersected with the restriction fragments of the reference genome and the read counts for each of the fragments are stored in the [fragcounts file](results/fragcounts/shha.fragcounts). The result of the smoothening is stored in [bedgraph files](results/smooth_data/shha_30frags_smooth.bedGraph) that can be uploaded to genome browser such as [UCSC](http://genome.ucsc.edu/). Below there is an example of the visualization in UCSC: 

![png](results/4C_snapshot.png) 

Finally, there is a [target file](results/targets/shha_targets.txt) that is also a bedfile like file containing those fragments significantly contacting the viewpoint and a fourth column with the p-value for the poisson test. 